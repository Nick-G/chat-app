import functools

from django.contrib.auth.models import AnonymousUser
from django.conf import settings
from django.contrib.sites.models import Site

from channels.handler import AsgiRequest
from rest_framework.authtoken.models import Token

from channels.sessions import channel_session


def token_user(func):
    @functools.wraps(func)
    def inner(message, *args, **kwargs):
        try:
            if "method" not in message.content:
                message.content['method'] = "FAKE"
            request = AsgiRequest(message)
        except Exception as e:
            raise ValueError("Cannot parse HTTP message - are you sure this is a HTTP consumer? %s" % e)

        token = request.GET.get("token")

        if token:
            try:
                user = Token.objects.get(key=token).user
            except Token.DoesNotExist:
                user = None
        else:
            user = None

        message.user = user
        message.token = token

        result = func(message, *args, **kwargs)
        return result
    return inner


def channel_session_user_from_token(func):
    @token_user
    @channel_session
    def inner(message, *args, **kwargs):
        if message.user is not None:
            message.channel_session['token'] = message.token
        return func(message, *args, **kwargs)
    return inner


def channel_token_user(func):
    @channel_session
    @functools.wraps(func)
    def inner(message, *args, **kwargs):
        if not hasattr(message, "channel_session"):
            raise ValueError("Did not see a channel session to get auth from")
        if message.channel_session is None:
            message.user = AnonymousUser()
        else:
            message.user = Token.objects.get(key=message.channel_session['token']).user
        # Run the consumer
        return func(message, *args, **kwargs)
    return inner


def get_protocol():
    if settings.SECURE_SSL_REDIRECT:
        return 'https://'
    else:
        return 'http://'


def get_full_path(relative_url):
    return '{}{}{}'.format(get_protocol(), Site.objects.get_current().domain, relative_url)
